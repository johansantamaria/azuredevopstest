﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yabber.Core.User.Entities;
using Yabber.Core.User.Models;
using Yabber.Core.User.Repository.Command;

namespace Yabber.Core.User.Managers
{
    public interface IUserService
    {
		Task<UserModel> SaveAsync(UserDto user);
		Task<IEnumerable<UserModel>> GetAllUsersAsync();
		Task<int> DeleteUserAsync(int userId);

	}
}
