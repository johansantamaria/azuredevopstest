﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Yabber.Core.User.Entities;
using Yabber.Core.User.Models;
using Yabber.Core.User.Repository.Command;
using Yabber.Core.User.Repository.Query;

[assembly: InternalsVisibleTo("Yabber.Core.User.UnitTest")]
namespace Yabber.Core.User.Managers.Implementation
{
	internal class UserService : IUserService
    {
		private readonly ICommandService _commandService;
		private readonly IQueriesService _queriesService;

		public UserService(ICommandService commandService, IQueriesService queriesService)
        {
            _commandService = commandService;
			_queriesService = queriesService;
		}

		public Task<int> DeleteUserAsync(int userId)
		{
			return _commandService.DeleteUserAsync(userId);
		}

		public Task<IEnumerable<UserModel>> GetAllUsersAsync()
		{
			return _queriesService.GetAllUsersAsync();
		}

		public async Task<UserModel> SaveAsync(UserDto user)
        {
			int createdUserId= await _commandService.SaveUserAsync(user);
			UserModel createdUser = await _queriesService.GetByIdAsync(createdUserId);
			return createdUser;

		}
    }
}
