﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Yabber.Core.User.Managers;
using Yabber.Core.User.Managers.Implementation;
using Yabber.Core.User.Repository.Command;
using Yabber.Core.User.Repository.Query;
using Yabber.Core.Utilities.ORM;
using Yabber.Core.Utilities.ORM.Implementation;

namespace Yabber.Core.User
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			// Microservice manager and repository classes
			services.AddScoped(typeof(IDapperBase<>), typeof(DapperBase<>));
			services.AddScoped<ICommandService, CommandService>();
			services.AddScoped<IQueriesService, QueriesService>();
			services.AddScoped<IUserService, UserService>();

			services.Configure<ApiBehaviorOptions>(options =>
			{
				options.SuppressModelStateInvalidFilter = true;
			});

			services.AddCors();
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

			services.AddSwaggerDocument();

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseCors(
				options => options.WithOrigins(Configuration["ClientHost"])
					.AllowAnyOrigin()
					.AllowAnyHeader()
					.AllowAnyMethod()
			);

			app.UseOpenApi();
			app.UseSwaggerUi3();

			app.UseHttpsRedirection();
			app.UseMvc();
		}
	}
}
