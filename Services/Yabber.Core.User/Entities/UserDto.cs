﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Yabber.Core.User.Entities
{
    public class UserDto : IValidatableObject
    {
        public UserDto()
        {

        }

        [Required(ErrorMessage = "First name is required.")]
        [StringLength(50, ErrorMessage = "First name field must not exceed 50 characters.")]
        public string firstName { get; set; }

        [Required(ErrorMessage = "Last name name is required.")]
        [StringLength(50, ErrorMessage = "Last name field must not exceed 50 characters.")]
        public string lastName { get; set; }

        [Required(ErrorMessage = "Birth date name is required.")]
        public DateTime birthDate { get; set; }

        [Required(ErrorMessage = "Address is required.")]
        [StringLength(50, ErrorMessage = "Address field must not exceed 50 characters.")]
        public string address { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [StringLength(50, ErrorMessage = "Email field must not exceed 50 characters.")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string email { get; set; }

        [Required(ErrorMessage = "Gender is required.")]
        public bool gender { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //BirthDate validation
            if (birthDate.Date > DateTime.Now.Date) {
                yield return new ValidationResult("Birth date must not be greater than the current date.", new List<string> { "birthDate" });
            }

        }
    }
}
