﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yabber.Core.User.Models;

namespace Yabber.Core.User.Repository.Query
{
    public interface IQueriesService
    {
        Task<IEnumerable<UserModel>> GetAllUsersAsync();
		Task<UserModel> GetByIdAsync(int userId);

	}
}
