﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Yabber.Core.User.Models;
using Yabber.Core.Utilities.Enums;
using Yabber.Core.Utilities.Helpers;
using Yabber.Core.Utilities.ORM;

namespace Yabber.Core.User.Repository.Query
{
	public class QueriesService : IQueriesService
    {
		private readonly IDapperBase<UserModel> _dapperUser;

		public QueriesService(IDapperBase<UserModel> dapperUser)
        {
			_dapperUser = dapperUser;
		}

        public async Task<IEnumerable<UserModel>> GetAllUsersAsync()
		{
			string getAllUsersScript = await ResourceHelper.GetResourceValue(ScriptType.Query, "GetAllUsers.sql");
			IEnumerable<UserModel> users = await _dapperUser.GetAllAsync(getAllUsersScript);
			return users;
		}

		public async Task<UserModel> GetByIdAsync(int userId)
		{
			string getUserByIdScript = await ResourceHelper.GetResourceValue(ScriptType.Query, "GetUserById.sql");
			return await _dapperUser.GetByIdAsync(getUserByIdScript, new { Id = userId });
		}
    }
}
