﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Yabber.Core.User.Entities;
using Yabber.Core.User.Models;
using Yabber.Core.Utilities.Enums;
using Yabber.Core.Utilities.Helpers;
using Yabber.Core.Utilities.ORM;

[assembly: InternalsVisibleTo("Yabber.Core.User.UnitTest")]
namespace Yabber.Core.User.Repository.Command
{
	public class CommandService : ICommandService
    {
		private readonly IDapperBase<UserModel> _dapperuser;

        public CommandService(IDapperBase<UserModel> dapperUser)
        {
			_dapperuser = dapperUser;
		}

		public async Task<int> DeleteUserAsync(int userId)
		{
			var parameters = new { Id = userId };
			string deleteUserSql = await ResourceHelper.GetResourceValue(ScriptType.Command, "DeleteUser.sql");
			int affectedRows = await _dapperuser.DeleteAsync(deleteUserSql, parameters);
			return affectedRows;
		}

		public async Task<int> SaveUserAsync(UserDto user)
        {
			var userParam = user.MapToParameters();
			string insertUserSql = await ResourceHelper.GetResourceValue(ScriptType.Command, "InsertUser.sql");
			int createdId = await _dapperuser.SaveAsync(insertUserSql, userParam);
			return createdId;
        }
    }
}
