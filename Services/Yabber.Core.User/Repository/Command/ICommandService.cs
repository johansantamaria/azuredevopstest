﻿using System.Threading.Tasks;
using Yabber.Core.User.Entities;
using Yabber.Core.User.Models;
using Yabber.Core.Utilities.Test.Unit;

namespace Yabber.Core.User.Repository.Command
{
	public interface ICommandService
    {
        Task<int> SaveUserAsync(UserDto user);
		Task<int> DeleteUserAsync(int userId);
    }
}
