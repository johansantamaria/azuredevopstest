﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yabber.Core.User.Entities;
using Yabber.Core.User.Managers;
using Yabber.Core.User.Models;

namespace Yabber.Core.User.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }


        [HttpPost]
        [Route("saveUser")]
        public async Task<IActionResult> SaveUserAsync([FromBody]UserDto user)
        {
            try {
                if (!ModelState.IsValid)
                {
                    var errors = ModelState.Select(v => new { key = v.Key, errors = v.Value.Errors.Select(y => y.ErrorMessage) });
                    return BadRequest(new { success = false, errors, message = "Invalid user." });
                }

                UserModel userCreated = await _userService.SaveAsync(user);

                dynamic response = new {
                    success = true, message = "User created.",
                    data = userCreated
                };

                return Ok(response);
            }
            catch(Exception exception){
                return BadRequest(new { success = false, message = exception.Message });
            }
        }

		[HttpGet]
		[Route("GetAllAsync")]
		public async Task<IActionResult> GetAllAsync()
		{
			IEnumerable<UserModel> users = await _userService.GetAllUsersAsync();
			var result = new { success = true, data = users };
			return Ok(result);
		}

		[HttpDelete]
		[Route("DeleteUserAsync")]
		public async Task<IActionResult> DeleteUserAsync([FromBody]int userId)
		{
			int affectedRows = await _userService.DeleteUserAsync(userId);
			var result = new { success = true, data = affectedRows };
			return Ok(result);
		}

	}
}