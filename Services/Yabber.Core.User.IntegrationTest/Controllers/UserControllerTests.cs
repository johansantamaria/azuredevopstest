﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Yabber.Core.User.Entities;
using Yabber.Core.User.Models;
using Yabber.Core.Utilities.Common.Entities;
using Yabber.Core.Utilities.Helpers;
using Yabber.Core.Utilities.Test.Integration;

namespace Yabber.Core.User.IntegrationTest.Controllers
{
	[TestFixture]
	public class UserControllerTests : IntegrationTestBase<Startup>
	{
		const string FIRST_NAME_VALUE = "Nserio";
		const string LAST_NAME_VALUE = "Corp";
		const string ADDRESS_VALUE= "809 E 40th St #4-1 Chicago, IL 60653";
		const string EMAIL_VALUE = "nserio@nserio.com";

		const string MICROSERVICE_NAMESPACE = "Yabber.Core.User";
		const string API_PREFIX = "/api/User/";

		public int CreatedUserId { get; set; }

		public UserControllerTests() : base(MICROSERVICE_NAMESPACE) {}

		[DatapointSource]
		public UserDto[] GetUserInputs =
		{
			new UserDto(){ },
			new UserDto(){ firstName=FIRST_NAME_VALUE },
			new UserDto(){ firstName=FIRST_NAME_VALUE, lastName=LAST_NAME_VALUE },
			new UserDto(){ firstName=FIRST_NAME_VALUE, lastName=LAST_NAME_VALUE, birthDate=DateTime.Now.AddMinutes(5) },
			new UserDto(){ firstName=FIRST_NAME_VALUE, lastName=LAST_NAME_VALUE, birthDate=DateTime.Now },
			new UserDto(){ firstName=FIRST_NAME_VALUE, lastName=LAST_NAME_VALUE, birthDate=DateTime.Now, address=ADDRESS_VALUE }
		};

		[Order(0)]
		[Theory]
		public async Task SaveUserAsync_ExpectedValidation_TestAsync(UserDto user)
		{
			HttpContent httpContent = ContentHelper.GetStringContent(user);
			HttpResponseMessage httpResponse = await _client.PostAsync($"{API_PREFIX}saveUser", httpContent);

			Assert.IsFalse(httpResponse.IsSuccessStatusCode);
		}

		[Order(1)]
		[Test]
		public async Task SaveUserAsync_Success_TestAsync()
		{
			UserDto user = new UserDto() {
				firstName = FIRST_NAME_VALUE,
				lastName = LAST_NAME_VALUE,
				birthDate = DateTime.Now,
				address = ADDRESS_VALUE,
				email = EMAIL_VALUE
			};
			HttpContent httpContent = ContentHelper.GetStringContent(user);
			HttpResponseMessage httpResponse = await _client.PostAsync($"{API_PREFIX}saveUser", httpContent);

			httpResponse.EnsureSuccessStatusCode();

			string parsedResponse = await httpResponse.Content.ReadAsStringAsync();
			var userCreatedResponse = JsonConvert.DeserializeObject<YabberCreationResponse<UserModel>>(parsedResponse);

			CreatedUserId = userCreatedResponse.data.Id;

			Assert.True(CreatedUserId != default(int));
		}

		[Order(2)]
		[Test]
		public async Task GetAllAsync_Success_TestAsync()
		{
			HttpResponseMessage httpResponse = await _client.GetAsync($"{API_PREFIX}GetAllAsync");
			httpResponse.EnsureSuccessStatusCode();

			string parsedResponse = await httpResponse.Content.ReadAsStringAsync();
			var usersResponse = JsonConvert.DeserializeObject<YabberQueryResponse<UserModel>>(parsedResponse);

			var createdUserFound = usersResponse.data.Where(user => user.Id == CreatedUserId).Any();

			Assert.True(createdUserFound);
		}

		[Order(3)]
		[Test]
		public async Task DeleteUserAsync_Success_TestAsync()
		{
			string clientAddress = _client.BaseAddress.ToString();
			string formattedAddress = clientAddress.Substring(0, clientAddress.Length-1);
			var request = new HttpRequestMessage
			{
				Method = HttpMethod.Delete,
				RequestUri = new Uri($"{formattedAddress}{API_PREFIX}DeleteUserAsync"),
				Content = ContentHelper.GetStringContent(CreatedUserId)
			};
			HttpResponseMessage httpResponse = await _client.SendAsync(request);
			httpResponse.EnsureSuccessStatusCode();

			string parsedResponse = await httpResponse.Content.ReadAsStringAsync();
			var deleteResponse = JsonConvert.DeserializeObject<YabberDeleteResponse>(parsedResponse);

			Assert.True(deleteResponse.data > 0);
		}

	}
}
