﻿namespace Yabber.Core.Utilities.Enums
{
	public enum ScriptType
	{
		Command,
		Query
	}
}
