﻿namespace Yabber.Core.Utilities.Common.Entities
{
	public class YabberCreationResponse<T> : YabberBaseResponse where T : class
	{
		public T data { get; set; }
	}
}
