﻿using System.Collections.Generic;

namespace Yabber.Core.Utilities.Common.Entities
{
	public class YabberBaseResponse
	{
		public bool success { get; set; }
		public IEnumerable<object> errors { get; set; }
		public string message { get; set; }
	}
}
