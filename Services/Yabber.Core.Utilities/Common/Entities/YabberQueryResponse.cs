﻿using System.Collections.Generic;

namespace Yabber.Core.Utilities.Common.Entities
{
	public class YabberQueryResponse<T> : YabberBaseResponse where T : class
	{
		public IEnumerable<T> data { get; set; }
	}
}
