﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yabber.Core.Utilities.Common.Entities
{
	public class YabberDeleteResponse : YabberBaseResponse
	{
		public int data { get; set; }
	}
}
