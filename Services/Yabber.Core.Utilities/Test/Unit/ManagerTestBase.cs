﻿using Moq;
using NUnit.Framework;
namespace Yabber.Core.Utilities.Test.Unit
{
	public abstract class ManagerTestBase<TestingClass> where TestingClass : class
	{
		protected TestingClass manager { get; set; }

		[OneTimeSetUp]
		public void TestInitialize()
		{
			Provision();
		}

		public virtual void Provision() { }

		internal void Prepare()
		{
			//Mock repositories (Command and query)
		}

	}
}
