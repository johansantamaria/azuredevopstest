﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.IO;
using System.Net.Http;

namespace Yabber.Core.Utilities.Test.Integration
{
	[TestFixture]
	public abstract class IntegrationTestBase<Startup> where Startup : class
	{
		protected HttpClient _client;
		protected TestServer _server;
		protected string _microServiceNamespace;

		public IntegrationTestBase(string microServiceNamespace)
		{
			_microServiceNamespace = microServiceNamespace;
		}

		[OneTimeSetUp]
		public void Setup()
		{
			var builder = new WebHostBuilder()
				.UseStartup<Startup>()
				.ConfigureAppConfiguration((context, config) =>
				{
					config.SetBasePath(Path.Combine(
						Directory.GetCurrentDirectory(),
						$"..\\..\\..\\..\\{_microServiceNamespace}"));

					config.AddJsonFile("appsettings.json");
				});

			_server = new TestServer(builder);

			_client = _server.CreateClient();
			_client.BaseAddress = new Uri("http://localhost:8888");
		}
	}
}
