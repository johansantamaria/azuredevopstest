﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace Yabber.Core.Utilities.Helpers
{
	public class ContentHelper
	{
		public static StringContent GetStringContent(object obj)
			=> new StringContent(JsonConvert.SerializeObject(obj), Encoding.Default, "application/json");
	}
}
