﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Yabber.Core.Utilities.Helpers
{
	public static class ORMHelper
	{
		public static object MapToParameters<T>(this T str)
		{
			var properties = typeof(T).GetProperties();
			dynamic parameters = new ExpandoObject();
			var parametersAsDictionary = (IDictionary<string, object>)parameters;
			foreach (var prop in properties)
			{
				parametersAsDictionary.Add(prop.Name ,prop.GetValue(str));
			}
			return parameters;
		}
	}
}
