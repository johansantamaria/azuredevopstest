﻿using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Yabber.Core.Utilities.Enums;

namespace Yabber.Core.Utilities.Helpers
{
	public static class ResourceHelper
	{
		public static Task<string> GetResourceValue(ScriptType type, string resourceFile)
		{
			Assembly assembly = Assembly.GetCallingAssembly();
			string assemblyName = assembly.GetName().Name;
			string typeName = string.Empty;
			switch (type)
			{
				case ScriptType.Command:
					typeName = "Commands";
					break;
				case ScriptType.Query:
					typeName = "Queries";
					break;
			}
			Stream resourceStream = assembly.GetManifestResourceStream($"{assemblyName}.Resources.{typeName}.{resourceFile}");
			using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
			{
				return reader.ReadToEndAsync();
			}
		}
	}
}
