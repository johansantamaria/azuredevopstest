﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yabber.Core.Utilities.ORM
{
	public interface IDapperBase<T>
	{
		Task<T> GetByIdAsync(string sql, object parameters = null);
		Task<IEnumerable<T>> GetAllAsync(string sql, object parameters = null);
		Task<int> SaveAsync(string sql, object parameters = null);
		Task<int> DeleteAsync(string sql, object parameters = null);
	}
}
