﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Yabber.Core.Utilities.ORM.Implementation
{
	public class DapperBase<T> : IDapperBase<T> where T : class
	{
		private SqlConnection sqlConnection;
		private readonly IConfiguration _configuration;

		public DapperBase(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		private async Task<SqlConnection> GetSqlConnection()
		{
			await Task.Yield();
			string connectionString = _configuration["ConnectionString"];
			return new SqlConnection(connectionString);
		}

		public async Task<int> SaveAsync(string sql, object parameters = null)
		{
			using (sqlConnection = await GetSqlConnection())
			{
				int createdId = await sqlConnection.QuerySingleOrDefaultAsync<int>(sql, parameters);
				return createdId;
			}
		}

		public async Task<int> DeleteAsync(string sql, object parameters = null)
		{
			using (sqlConnection = await GetSqlConnection())
			{
				int affectedRows = await sqlConnection.ExecuteAsync(sql, parameters);
				return affectedRows;
			}
		}

		public async Task<IEnumerable<T>> GetAllAsync(string sql, object parameters = null)
		{
			using (sqlConnection = await GetSqlConnection())
			{
				IEnumerable<T> objects = await sqlConnection.QueryAsync<T>(sql, parameters);
				return objects;
			}
		}

		public async Task<T> GetByIdAsync(string sql, object parameters = null)
		{
			using (sqlConnection = await GetSqlConnection())
			{
				T model = await sqlConnection.QuerySingleOrDefaultAsync<T>(sql, parameters);
				return model;
			}
		}
	}
}
