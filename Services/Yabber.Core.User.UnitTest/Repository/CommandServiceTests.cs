﻿using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using Yabber.Core.User.Entities;
using Yabber.Core.User.Models;
using Yabber.Core.User.Repository.Command;
using Yabber.Core.Utilities.ORM;

namespace Yabber.Core.User.UnitTest.Repository
{
	public class CommandServiceTests
	{
		private ICommandService commandService;

		const string SQL = "INSERT INTO USERS";
		const int CREATED_USER_ID= 100;

		[OneTimeSetUp]
		public void SetUp()
		{
			var dapperBaseMock = new Mock<IDapperBase<UserModel>>();

			dapperBaseMock.Setup(p => p.SaveAsync(It.IsAny<string>(), It.IsAny<object>()))
				.Returns<string, object>(async (sql, parameters) =>
				{
					await Task.Yield();
					return CREATED_USER_ID;
				});

			commandService = new CommandService(dapperBaseMock.Object);
		}

		[Test]
		public async Task SaveUserAsync_Success_TestAsync()
		{
			UserDto user = new UserDto();
			int createdUserId = await commandService.SaveUserAsync(user);
			Assert.AreEqual(CREATED_USER_ID, createdUserId);
		}
	}
}
