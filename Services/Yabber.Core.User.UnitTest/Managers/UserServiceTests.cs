﻿using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using Yabber.Core.User.Entities;
using Yabber.Core.User.Managers;
using Yabber.Core.User.Managers.Implementation;
using Yabber.Core.User.Models;
using Yabber.Core.User.Repository.Command;
using Yabber.Core.User.Repository.Query;
using Yabber.Core.Utilities.Test.Unit;

namespace Yabber.Core.User.UnitTest.Managers
{
	[TestFixture]
	public class UserServiceTests : ManagerTestBase<IUserService>
	{
		const int CREATED_USER_ID = 100;
		public override void Provision()
		{
			var mockCommandService = new Mock<ICommandService>();
			mockCommandService
				.Setup(p=>p.SaveUserAsync(It.IsAny<UserDto>()))
				.Returns<UserDto>(async (user) => {
					await Task.Yield();
					return CREATED_USER_ID;
				});

			var mockQueriesService = new Mock<IQueriesService>();
			mockQueriesService.Setup(p=>p.GetByIdAsync(It.IsAny<int>()))
				.Returns<int>(async (userId) => {
					await Task.Yield();
					var userCreated = new UserModel()
					{
						Id = CREATED_USER_ID,
					};
					return userCreated;
				});

			manager = new UserService(mockCommandService.Object, mockQueriesService.Object);
		}

		[Test]
		public async Task SaveAsync_Success_TestAsync()
		{
			var user = new UserDto();
			UserModel createdUser = await manager.SaveAsync(user);
			Assert.AreEqual(CREATED_USER_ID, createdUser.Id);
		}
	}
}
