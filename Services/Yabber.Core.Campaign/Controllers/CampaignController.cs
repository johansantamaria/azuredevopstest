﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yabber.Core.Campaign.Entities;
using Yabber.Core.Campaign.Managers;
using Yabber.Core.Campaign.Models;

namespace Yabber.Core.Campaign.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class CampaignController : ControllerBase
    {
        private static readonly string[] Campaigns = new string[] {
            "Campaign 1", "Campaign 2"
        };
		private readonly ICampaignService _campaignService;

		public CampaignController(ICampaignService campaignService)
		{
			_campaignService = campaignService;
		}

        /// <summary>
        /// This is an additional documentation
        /// </summary>
        /// <returns>List of string</returns>
        [HttpGet]
        [Route("GetCampaigns")]
        public ActionResult<IEnumerable<string>> GetCampaigns()
        {
            return Campaigns;
        }

        [HttpGet]
        [Route("GetCampaign")]
        public ActionResult<string> GetCampaign(int id)
        {
            return Campaigns[id];
        }

        [HttpPost]
		[Route("SaveCampaignAsync")]
		public async Task<IActionResult> SaveCampaignAsync([FromBody]CampaignDto campaign)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					var errors = ModelState.Select(v => new { key = v.Key, errors = v.Value.Errors.Select(y => y.ErrorMessage) });
					return BadRequest(new { success = false, errors, message = "Invalid campaign." });
				}

				CampaignModel campaignCreated = await _campaignService.SaveAsync(campaign);

				dynamic response = new
				{
					success = true,
					message = "Campaign created.",
					data = new { campaignCreated }
				};

				return Ok(response);
			}
			catch (Exception exception)
			{
				return BadRequest(new { success = false, message = exception.Message });
			}
		}

		[HttpGet]
		[Route("GetAllCampaignsAsync")]
		public async Task<IActionResult> GetAllCampaignsAsync()
		{
			IEnumerable<CampaignModel> campaigns = await _campaignService.GetAllAsync();
			var result = new { success = true, data = campaigns };
			return Ok(result);
		}
	}
}