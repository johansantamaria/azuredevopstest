﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Yabber.Core.Campaign.Entities
{
    public class CampaignDto
    {
        public CampaignDto()
        {

        }

        [Required(ErrorMessage = "name is required.")]
        [StringLength(50, ErrorMessage = "Name field must not exceed 50 characters.")]
        public string name { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        [StringLength(200, ErrorMessage = "Description field must not exceed 200 characters.")]
        public string description { get; set; }
	}
}
