﻿using System.Threading.Tasks;
using Yabber.Core.Campaign.Entities;
using Yabber.Core.Campaign.Models;

namespace Yabber.Core.Campaign.Repository.Command
{
	public interface ICommandService
    {
        Task<int> SaveAsync(CampaignDto campaign);
    }
}
