﻿using System.Threading.Tasks;
using Yabber.Core.Campaign.Entities;
using Yabber.Core.Campaign.Models;
using Yabber.Core.Campaign.Resources;
using Yabber.Core.Utilities.ORM;
using Yabber.Core.Utilities.Helpers;

namespace Yabber.Core.Campaign.Repository.Command
{
	public class CommandService : ICommandService
    {
		private readonly IDapperBase<CampaignModel> _dapperCampaign;

        public CommandService(IDapperBase<CampaignModel> dapperCampaign)
        {
			_dapperCampaign = dapperCampaign;
		}

		public async Task<int> SaveAsync(CampaignDto campaign)
		{
			var userParam = campaign.MapToParameters();
			int createdId = await _dapperCampaign.SaveAsync(Scripts.InsertCampaign, userParam);
			return createdId;
		}
	}
}
