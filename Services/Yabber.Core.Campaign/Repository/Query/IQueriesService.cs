﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yabber.Core.Campaign.Models;

namespace Yabber.Core.Campaign.Repository.Query
{
	public interface IQueriesService
    {
        Task<IEnumerable<CampaignModel>> GetAllAsync();
		Task<CampaignModel> GetByIdAsync(int campaignId);
	}
}
