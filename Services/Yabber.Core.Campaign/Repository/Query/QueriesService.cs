﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yabber.Core.Campaign.Models;
using Yabber.Core.Campaign.Resources;
using Yabber.Core.Utilities.ORM;

namespace Yabber.Core.Campaign.Repository.Query
{
	public class QueriesService : IQueriesService
	{
		private readonly IDapperBase<CampaignModel> _dapperCampaign;

		public QueriesService(IDapperBase<CampaignModel> dapperCamapign)
        {
			_dapperCampaign = dapperCamapign;
		}

		public async Task<IEnumerable<CampaignModel>> GetAllAsync()
		{
			IEnumerable<CampaignModel> users = await _dapperCampaign.GetAllAsync(Scripts.GetAllCampaigns);
			return users;
		}

		public Task<CampaignModel> GetByIdAsync(int campaignId)
		{
			return _dapperCampaign.GetByIdAsync(Scripts.GetCampaignById, new { Id = campaignId });
		}
	}
}
