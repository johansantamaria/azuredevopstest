﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yabber.Core.Campaign.Entities;
using Yabber.Core.Campaign.Models;

namespace Yabber.Core.Campaign.Managers
{
	public interface ICampaignService
    {
		Task<CampaignModel> SaveAsync(CampaignDto user);
		Task<IEnumerable<CampaignModel>> GetAllAsync();

	}
}
