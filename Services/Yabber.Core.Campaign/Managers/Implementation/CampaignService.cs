﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yabber.Core.Campaign.Entities;
using Yabber.Core.Campaign.Models;
using Yabber.Core.Campaign.Repository.Command;
using Yabber.Core.Campaign.Repository.Query;

namespace Yabber.Core.Campaign.Managers.Implementation
{
	internal class CampaignService : ICampaignService
    {
		private readonly ICommandService _commandService;
		private readonly IQueriesService _queriesService;

		public CampaignService(ICommandService commandService, IQueriesService queriesService)
        {
            _commandService = commandService;
			_queriesService = queriesService;
		}

		public Task<IEnumerable<CampaignModel>> GetAllAsync()
		{
			return _queriesService.GetAllAsync();
		}

		public async Task<CampaignModel> SaveAsync(CampaignDto user)
		{
			int createdCampaignId = await _commandService.SaveAsync(user);
			CampaignModel createdCampaign = await _queriesService.GetByIdAsync(createdCampaignId);
			return createdCampaign;
		}
	}
}
