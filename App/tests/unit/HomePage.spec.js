import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import HomePage from '../../src/components/pages/home/HomePage';
import getters from '../../src/store/modules/home/getters';


const welcomeMessage = "Welcome to Yabber Project";

const localVue = createLocalVue()

localVue.use(Vuex);

describe('HomePage', () => {
    let store;

    beforeEach(() => {
        store = new Vuex.Store({
            modules: {
                home: {
                    state: { 
                        testMessage: welcomeMessage 
                    }
                }

            }
        });
    });

    it('Should contains a welcome message', () => {
        const wrapper = shallowMount(HomePage, { store, localVue });

        expect(wrapper.text()).toMatch(welcomeMessage);
    });

    it('getter should return message to upper case', () => {
        const state = {
            testMessage: welcomeMessage
        };
        expect(getters.getTestMessageToUpper(state)).toMatch(welcomeMessage.toUpperCase());
    });
});