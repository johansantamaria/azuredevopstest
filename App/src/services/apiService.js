import axios from 'axios';

const baseURL = `${process.env.VUE_APP_API_SERVICE_HOST}/api`;

const ApiService = axios.create({
    baseURL,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
    }
});


const errorHandler = (error) => ({
    status : error.response.status,
    success : error.response.data.success,
    data : error.response.data.data,
    message : error.response.data.message,
    errors : error.response.data.errors
});

const successHandler = (response) => ({
    status : response.status,
    success : response.data.success,
    data : response.data.data,
    message : response.data.message,
    errors : response.data.errors ? response.data.errors : []
});

ApiService.interceptors.response.use(
    (response) => successHandler(response),
    (error) => errorHandler(error)
);

// Const by microservices
const UserURL = {
    SaveURL: '/user/saveUser',
};

export { ApiService, UserURL }