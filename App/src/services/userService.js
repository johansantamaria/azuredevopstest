import {ApiService, UserURL} from './apiService';

export const UserService  = {
    GetAllAsync(){
        return new Promise((resolve) => {
            setTimeout(function() {
                var users=[
                    { firstName:"Aaron", lastName:"Machado" },
                    { firstName:"Camilo", lastName:"Torres" },
                    { firstName:"David", lastName:"Solorzano" },
                    { firstName:"Johan", lastName:"Santamaria" }
                ];
                resolve(users);
            }, 1000);
        });
    },
    AddAsync(user){
        return ApiService.post(UserURL.SaveURL, user);
    }
};