import { UserService } from '../../../services/userService'

export default {
    async getAllUsersAsync({commit, getters}){
        let result= await UserService.GetAllAsync();
        if(result){
            var mappedUsers=getters.mapUsers(result);
            commit("setUsers", mappedUsers);
        }
    },
    async addUserAsync({commit, getters}, user){
        let result= await UserService.AddAsync(user);
        if(result){
            var mappedUsers=getters.mapUsers([user]);
            commit("addUser", mappedUsers[0]);
        }
    }
}