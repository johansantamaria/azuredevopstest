import getters from './getters';
import actions from './actions';
import mutations from './mutations';

const state = {
    testMessage: "Welcome to Yabber Project",
    apiHost: "",
    users:[]
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}