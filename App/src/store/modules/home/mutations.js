import { MutationsHelper } from '../../helpers';

export default {
    setUsers: MutationsHelper.set('users'),
    addUser: MutationsHelper.pushTo('users')
}