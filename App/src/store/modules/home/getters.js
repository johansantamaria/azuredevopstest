export default {
    getTestMessageToUpper: (state) => {
        var environmentMesage = ` from ${process.env.NODE_ENV}`;
        var message=`${state.testMessage}${environmentMesage}`;
        return message.toUpperCase();
    },
    getApiHost: () => {
        return process.env.VUE_APP_API_SERVICE_HOST;
    },
    mapUsers: () => (usersData) => {
        usersData.forEach(user => {
            user.fullName=`${user.firstName} ${user.lastName}`;
        });
        return usersData;
    }
}