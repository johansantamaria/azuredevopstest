import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '../components/pages/home/HomePage';
import UserForm from '../components/pages/user/UserForm';

Vue.use(Router);

export default new Router({
    routes:[{
        path: '/',
        name: 'Home',
        component: HomePage
    },
    {
        path: '/User',
        name: 'User',
        component: UserForm
    }]
});